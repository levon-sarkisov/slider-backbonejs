import $ from 'jquery';
import _ from 'underscore';
import Backbone from 'backbone';
import SliderModel from '../../models/slider';

const SliderCollection = Backbone.Collection.extend({
  model: SliderModel,
  url: './data.json'
});

export default SliderCollection;
