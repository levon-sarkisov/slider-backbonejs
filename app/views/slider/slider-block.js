import $ from 'jquery';
import _ from 'underscore';
import Backbone from 'backbone';

const SliderRowView = Backbone.View.extend({
  tagName: 'li',

  template: _.template($('#slider-row-tmp').html()),
  
  initialize: function() {
    this.render();
  },

  render: function(n) {
    this.$el.html(this.template(this.model));
    return this;
  }
});

export default SliderRowView;
