import $ from 'jquery';
import _ from 'underscore';
import Backbone from 'backbone';
import SliderCollection from '../../collections/slider';
import SliderRowView from './slider-block';

const SliderView = Backbone.View.extend ({
  el: '.slider',
  
  events: {
    'click .slider__left': 'onLeft',
    'click .slider__right': 'onRight'
  },
  
  initialize: function() {
    this.max = 0;
    this.current = 0;
    
    this.$thumbnails = this.$el.find('.thumbnails');
    
    this.collection = new SliderCollection();
    this.collection.fetch({success: _.bind(this.render, this)});
    this.listenTo(this.collection, 'add', this.updateCollection);
  },
  
  render: function(self, model) {
    this.$el.addClass('disable-left');
    
    this.max = model.length - 1;
    this.$thumbnails.width(model.length * 100 + '%');
    
    model.forEach((item, i) => {
      const block = new SliderRowView({ model: item });
      block.$el.width(100 / model.length + '%');
      this.$thumbnails.append(block.el);
    });
  },
  
  moveSlider: function(e) {
    this.$thumbnails.css({
      left: '-' + 100 * this.current + '%'
    });
  },
  
  onLeft: function(e) {
    if(this.$el.is('.disable-right')) {
      this.$el.removeClass('disable-right');
    }
    
    if(this.current === 1) {
      this.$el.addClass('disable-left');
    }

    if(this.current > 0) {
      this.current -= 1;
      this.moveSlider();
    }
  },
  
  onRight: function(e) {
    if(this.$el.is('.disable-left')) {
      this.$el.removeClass('disable-left');
    }
    
    if(this.current === (this.max - 1)) {
      this.$el.addClass('disable-right');
    }
    
    if(this.current < this.max) {
      this.current += 1;
      this.moveSlider();
    }
  }
});

export default SliderView;
