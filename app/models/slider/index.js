import $ from 'jquery';
import _ from 'underscore';
import Backbone from 'backbone';

const SliderModel = Backbone.Model.extend({
  defaults: {
    title: '',
    images: []
  }
});

export default SliderModel;
