import $ from 'jquery';
import _ from 'underscore';
import './assets/css/styles.scss';
import { Model, View, Controller, Router } from 'backbone';
import SliderView from './views/slider';

new SliderView();