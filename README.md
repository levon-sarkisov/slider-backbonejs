# README #

This is simple slider writen on Backbone and ES6
====

For run this project you need to have installed *nodejs*
After downloading project in root directory please make these steps:

Open terminal and run

* *npm i*
* *npm start*
* *open in browser http://localhost:3000*
